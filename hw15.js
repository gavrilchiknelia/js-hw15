$(document).ready(function(){
    $("#menu").on("click","a", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();
        //забираем идентификатор бока с атрибута href
        let id  = $(this).attr('href'),
        //узнаем высоту от начала страницы до блока на который ссылается якорь
        top = $(id).offset().top;
        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({scrollTop: top}, 1500);
    });
});
$(window).scroll(function() {

    if ($(window).scrollTop() > window.innerHeight) {
        $('#button').addClass('show');
    } else {
        $('#button').removeClass('show');
    }
});
(function($) {
    $(function() {

        $('#button').click(function() {
            $('html, body').animate({scrollTop: 0},1000);
            return false;
        })

    })
})(jQuery);
(function($) {
    $(function() {

        $('#slideToggle').on('click', function (event) {
            $('#newsHide').slideToggle();
            if(this.textContent ==="Hide"){
                this.textContent = "Show";
            }
            else {
                this.textContent = "Hide";
            }

        });
    })
})(jQuery);